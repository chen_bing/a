package com.example.broadcastbestpractice;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.WindowManager;
import android.widget.DialerFilter;

public class ForceOfflineReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(final Context context, Intent intent) {
		AlertDialog.Builder diaBuilder = new AlertDialog.Builder(context);
		diaBuilder.setTitle("Warning");
		diaBuilder.setMessage("You are forced to be offline. Please try to login again.");
		diaBuilder.setCancelable(false);
		diaBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ActivityCollector.finshAll();
				Intent intent = new Intent(context, LoginActivity.class);
				intent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}
		});
		
		//diaBuilder.show();	//runtime error
		AlertDialog alertDialog = diaBuilder.create();
		alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		alertDialog.show();		
	}

	
}
